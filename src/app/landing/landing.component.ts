import { Component, OnInit } from '@angular/core';

import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  constructor() { }
  ngOnInit() {  }


  name = 'AJ\'s AnyObject Library Search';
  searchType = 'Buy'; search = '';
  typeOfProptySelected = 'none'; districtSelected = 'none'; bedrmsSelected = 'none';
  public imageSources: string[] = [
    'assets/images/Affinity-at-Serangoon-Singapore-Singapore.jpg',
    'assets/images/Concourse-Skyline-Singapore-Singapore.jpg',
    'assets/images/Margaret-Ville-Alexandra-Commonwealth-Singapore.jpg',
    'assets/images/Sea-Pavilion-Residences-Singapore-Singapore.jpg',
    'assets/images/The-Garden-Residences-Singapore-Singapore.jpg',
  ]




  public findBooks(searchType: string, searchStr: string) {

  }

  public backHome(searchStr: string) {

  }


}
